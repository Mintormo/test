

import sys, os
import win32api, win32con
from win32com.shell import shell, shellcon
import PyQt4.QtGui

QtGui = PyQt4.QtGui

project_settings_folder = "prj"
last_opened_files       = "last_opened_files"

projectfolder = None
projectname   = None

projecttype   = None
compilemode   = None

def CallbackFolderDialog(hwnd,msg,lp,data):
 global projectfolder

 pidl = shell.AddressAsPIDL(lp)
 try:
  path = shell.SHGetPathFromIDList(pidl)
 except shell.error:
  pass
 projectfolder = path.decode("cp1251").lower()

def CreateProject(mainwin):
 pid, folder, image = shell.SHBrowseForFolder(0,None,"Выберите папку для проекта",
                                              64, # 64 - означает, что у диалога есть кнопка создания папки
                                              CallbackFolderDialog,None)
 global projectname
 global projectfolder
 global compilemode
 global projecttype

 if folder is not None:
  if not os.access(projectfolder+"\\debug",os.R_OK):
   os.mkdir(projectfolder+"\\debug")
  if not os.access(projectfolder+"\\release",os.R_OK):
   os.mkdir(projectfolder+"\\release")
  if not os.access(projectfolder+"\\"+project_settings_folder,os.R_OK):
   os.mkdir(projectfolder+"\\prj")
  f = open(projectfolder+"\\project.kwp","w",encoding="utf8")
  s = "ProjectType=cui\n"+\
      "CompileMode=debug"
  f.write(s)
  f.close()
  projecttype = "cui"
  compilemode = "debug"
  mainwin.CuiItem.setChecked(True)
  mainwin.DebugPrjItem.setChecked(True)
  mainwin.ProjectType.setText("  CUI  ")
  mainwin.ProjectVersion.setText("  Отладочная версия  ")
  mainwin.setWindowTitle("["+folder.lower()+"] - Kwinta")
  projectname = folder[:].lower()
  mainwin.tabs.ClearAll()
  mainwin.Timer.start()

def OpenProject(mainwin):
 global projectfolder
 global projectname
 global projecttype
 global compilemode

 d = QtGui.QFileDialog()
 s = d.getOpenFileName(None,"Выберите файл проекта","","Файл проекта Kwinta (*.kwp)")
 if s != "":
  mainwin.Timer.stop()
  mainwin.tabs.ClearAll()

  s = s.replace("/","\\")
  a = os.path.split(s)
  projectfolder = a[0][:]
  projectfolder = projectfolder.lower()
  projectname = os.path.split(projectfolder)[1][:]
  mainwin.setWindowTitle("["+projectname+"] - Kwinta")
  f = open(projectfolder+"\\project.kwp","r",encoding="utf8")
  projecttype = "cui"
  compilemode = "debug"
  for t in f:
   param = t.split("=")
   param[0] = param[0].lower()
   # При разбивке строки param[1] в ее хвост попадает символ переноса строки.
   # Его приходится убирать отдельно.
   param[1] = param[1].lower().split("\n")[0]
   if param[0] == "projecttype":
    projecttype = param[1][:]
    if projecttype == "cui":
     mainwin.CuiItem.setChecked(True)
     mainwin.ProjectType.setText("  CUI  ")
    elif projecttype == "gui":
     mainwin.GuiItem.setChecked(True)
     mainwin.ProjectType.setText("  GUI  ")
    elif projecttype == "dll":
     mainwin.DllItem.setChecked(True)
     mainwin.ProjectType.setText("  DLL  ")
    elif projecttype == "lib":
     mainwin.LibItem.setChecked(True)
     mainwin.ProjectType.setText("  LIB  ")
    elif projecttype == "obj":
     mainwin.ObjItem.setChecked(True)
     mainwin.ProjectType.setText("  OBJ  ")
    else:
     mainwin.CuiItem.setChecked(True)
     mainwin.ProjectType.setText("  CUI  ")
   elif param[0] == "compilemode":
    compilemode = param[1][:]
    if compilemode == "debug":
     mainwin.DebugPrjItem.setChecked(True)
     mainwin.ProjectVersion.setText("  Отладочная версия  ")
    elif compilemode == "release":
     mainwin.ReleasePrjItem.setChecked(True)
     mainwin.ProjectVersion.setText("  Финальная версия  ")
    else:
     mainwin.DebugPrjItem.setChecked(True)
     mainwin.ProjectVersion.setText("  Отладочная версия  ")

  f.close()
  if mainwin.ListOfFiles is not None:
   mainwin.ListOfFiles.Refresh()
  if os.access(projectfolder+"\\"+project_settings_folder+"\\"+last_opened_files,os.F_OK):
   f = open(projectfolder+"\\"+project_settings_folder+"\\"+last_opened_files,"r",encoding="utf8")
   current_index = -1
   for l in f:
    p = l.split(",")
    n = os.path.split(p[0])
    if os.access(p[0],os.F_OK):
     strings = OpenFile(mainwin,p[0])
     if strings is not None:
      mainwin.tabs.AddTab(n[1],p[0])
      if len(mainwin.tabs.editors) > 0:
       mainwin.tabs.editors[len(mainwin.tabs.editors)-1].editor.setText(strings)
       mainwin.tabs.editors[len(mainwin.tabs.editors)-1].editor.setCursorPosition(int(p[1]),int(p[2]))
       mainwin.tabs.editors[len(mainwin.tabs.editors)-1].editor.setModified(False) 
      else:
       mainwin.tabs.editors[0].editor.setText(strings)
       mainwin.tabs.editors[0].editor.setCursorPosition(int(p[1]),int(p[2]))
       mainwin.tabs.editors[0].editor.setModified(False)
 
      # Установлена метка текущей вкладки 
      if len(p) == 4:  
       current_index = len(mainwin.tabs.paths)-1
   f.close()
   if current_index != -1:
    mainwin.tabs.setCurrentIndex(current_index)
   else:
    mainwin.tabs.setCurrentIndex(0)
  mainwin.Timer.start()

def SaveProject(mainwin):
 global projecttype
 global compilemode

 f = open(projectfolder+"\\project.kwp","wb")
 s = "ProjectType="+projecttype+"\nCompileMode="+compilemode
 s = s.encode("utf8")
 f.write(s)
 f.close()
 f = open(projectfolder+"\\"+project_settings_folder+"\\last_opened_files","w",encoding="utf8")
 for i in range(mainwin.tabs.count()):
  line,index = mainwin.tabs.editors[i].editor.getCursorPosition()
  if mainwin.tabs.count() > 0 and mainwin.tabs.currentIndex() == i:
   f.write(mainwin.tabs.paths[i]+","+str(line)+","+str(index)+",+\n")
  else:
   f.write(mainwin.tabs.paths[i]+","+str(line)+","+str(index)+"\n")
 f.close()

def OpenFile(mainwin,filename):
 try:
  f = open(filename,"rb")
  strings = f.read()
  f.close()
  strings = strings.decode("utf8")
 except IOError:
  win32api.MessageBox(0,"Не удалось открыть файл \""+filename+"\".","Ошибка",\
                      win32con.MB_OK+win32con.MB_ICONERROR)
  f.close()
  return None
 except UnicodeDecodeError:
  f.close()
  win32api.MessageBox(0,"Не удалось открыть \""+filename+"\".\n"+
                        "Кодировка файла должна быть UTF-8.","Ошибка",
                         win32con.MB_OK+win32con.MB_ICONERROR)
  return None
 return strings

def SaveFiles(mainwin):
 mainwin.Timer.stop()
 if mainwin.tabs.count() > 0:
  for i in range(len(mainwin.tabs.pages)):
   if mainwin.tabs.editors[i].editor.isModified():
    SaveFile(mainwin,i)
 mainwin.Timer.start()

def SaveFile(mainwin,index):
 try:
  f = open(mainwin.tabs.paths[index],"wb")
 except IOError:
  win32api.MessageBox(0,"Не удалось открыть файл \""+filename+"\".","Ошибка",\
                      win32con.MB_OK+win32con.MB_ICONERROR)
  f.close()
  return
 text = mainwin.tabs.editors[index].editor.text()
 text = text.encode("utf8")
 # Добавляем BOM
 text = b"\xEF\xBB\xBF"+text
 f.write(text)
 f.close()


