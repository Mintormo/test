import sys, os
import win32api, subprocess, win32con
import PyQt4.QtGui, PyQt4.QtCore
import project

QtGui = PyQt4.QtGui
QtCore = PyQt4.QtCore

include_paths = ["/Id:\\igor\\tools\\include"]
lib_paths     = ["/LIBPATH:d:\\igor\\tools\\lib"] # 1 2

compiler_path = "d:\\igor\\tools\\cl.exe"
linker_path   = "d:\\igor\\tools\\link.exe" # 5
lib_path      = "d:\\igor\\tools\\lib.exe"
rc_path       = "d:\\igor\\tools\\rc.exe" # 3 4



def SyntaxCheckFile(mainwin,path):
 project.SaveFiles(mainwin)
 f = subprocess.Popen([compiler_path,include_paths[0],"/I"+project.projectfolder,"/c","/Zs","/EHsc",path],
                       stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE, shell=True)
 out,err = f.communicate()
 out = out.decode("cp866")
 lines = out.splitlines()
 errors = []
 warnings = []
 for s in lines:
  if s.find("error") != -1:
   errors.append(s)
   continue
  if s.find("warning") != -1:
   warnings.append(s)
 if len(errors) > 0 or len(warnings) > 0:
  for i in range(len(errors)):
   m = QtGui.QTreeWidgetItem(mainwin.OutputWindow.ErrorsTree)
   m.setToolTip(0,errors[i])
   m.setText(0,errors[i])
  for i in range(len(warnings)):
   m = QtGui.QTreeWidgetItem(mainwin.OutputWindow.MessagesTree)
   m.setText(0,warnings[i])
  #mainwin.OutputWindow.show()
  return len(errors),len(warnings)
 else:
  win32api.MessageBox(0,"Ошибок и предупреждений нет.","Результат проверки",
                      win32con.MB_OK+win32con.MB_ICONINFORMATION) 
