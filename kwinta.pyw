﻿
import sys
import PyQt4.QtGui,win32api,win32con,PyQt4.QtCore
import ui
import text

QtGui = PyQt4.QtGui
QtCore = PyQt4.QtCore

t = QtCore.QTranslator()
t.load("qt_ru",".\\")

try:
 win32api.LoadLibrary("scilexer.dll") 
except:
 win32api.MessageBox(0,text.ErrorLoadScintilla,
           text.ErrorWndTitle,win32con.MB_OK | win32con.MB_ICONERROR)
else:
 app = QtGui.QApplication(sys.argv)
 app.installTranslator(t)
 main = ui.MainWindow()
# app.setStyle("WindowsXP")
 sys.exit(app.exec_())
